# Exam project

Big Data course (81932), University of Bologna.

Student: Simone Letizi - 887396

sletizi@isi-vclust3
### Dataset
https://www.kaggle.com/aestheteaman01/covid19-literary-analysis-dataset-covlad##COVID-Literature-Analysis.csv

### Jobs: Execution commands
All the following commands must be launched from the folder containing the jar
* **QUERY 1**: After identifying the most important 100 journals (based on the number of publications), draw up the ranking of the most active authors in the most important journals.
    * **MapReduce** implementation
        ```sh
        $ hadoop jar <jarFile> <mainClass> [<numReducerFirstJob> <numReducerTaskThirdJob> <useCombiner>]
        ```       
        Full example: 
        ```sh
        $ hadoop jar BDE-mr-letizi-all.jar MapReduceApp 1 1 true
        ```
    * **Spark** implementation
        ```sh
        $ spark2-submit --class mainClass <jarFile> [<parquetFileInputPath> <TXTFileJournalsRankOutputPath> <TXTFileAuthorsRankOutputPath> <joinType> <usePartitioningWithNPartitions> <useCaching>]
        ```
        Parameters:
        * joinType: 1(Spark join), 2(broadcast variable)
        * usePartitioningWithNPartitions: 0 to avoid using partitioner; N to use partitioner with N partitions
        * useCaching: true for use caching, false otherwise
        
        Full example (if no parameters were specified (2-12-true) are used by default): 
        ```sh
        $ spark2-submit --class query1.AuthorsRankJobSpark BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/spark/output/authors_rank.txt
        ```
    * **SparkSQL** implementation
        ```sh
        $ spark2-submit --class mainClass <jarFile> [<parquetFileInputPath> <CSVFileAuthorsRankOutputPath>]
        ```
        Full example: 
        ```sh
        $ spark2-submit --class query1.AuthorsRankJobSparkSQL BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/sparkSQL/output/authors_rank.csv 
        ```
* **QUERY 2**: 
    * **SparkSQL** implementation
        ```sh
            $ spark2-submit --class mainClass <jarFile> [<parquetFileInputPath> <CSVFileOutputPathMeanTF> <CSVFileOutputPathMeanOcc> <stopWordsFile>]
        ```
        Full example: 
        ```sh
        $ spark2-submit --class query2.MeanFreqWordPerCategorySparkSQL BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/sparkSQL/output/WordMeanTFPerCategory.csv hdfs:/user/sletizi/examProject/sparkSQL/output/WordMeanOccurrencesPerCategory.csv hdfs:/user/sletizi/examProject/dataset/stopwords.txt
        ```
