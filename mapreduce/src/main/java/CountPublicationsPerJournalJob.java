import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.example.ExampleInputFormat;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static utils.DatasetFieldsConstants.COLUMN_JOURNAL;

public class CountPublicationsPerJournalJob {

    private static final Log LOGGER = LogFactory.getLog(CountPublicationsPerJournalJob.class);
    private static final String JOB_NAME = "CountPublicationsPerJournalJob";

    public static Job getJob(Configuration conf, int numberReduceTasks, Path inputPath, Path outputPath, boolean useCombiner) throws IOException {
        LOGGER.info("GetJob" + JOB_NAME);
        Job job = Job.getInstance(conf, JOB_NAME);

        // Clear output folder
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputPath)) {
            LOGGER.info(JOB_NAME+" - Deleting already existing output file");
            hdfs.delete(outputPath, true);
        }

        // Read parquet format
        job.setInputFormatClass(ExampleInputFormat.class);
        job.setJarByClass(CountPublicationsPerJournalJob.class);

        // set number of reducer tasks
        job.setNumReduceTasks(numberReduceTasks);

        // set mapper, reducer and combiner class
        job.setMapperClass(CountJournalOccurrenceMapper.class);
        if(useCombiner){
            job.setCombinerClass(OccurrencesSumReducer.class);
        }
        job.setReducerClass(OccurrencesSumReducer.class);

        // set output (k,v) type
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // set input and output path
        FileInputFormat.addInputPath(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);

        return job;
    }

    static class CountJournalOccurrenceMapper extends Mapper<Object, Group, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text journalWritable = new Text();

        //UNCOMMENT FOR IN-MAPPER COMBINER
        //private Map<String, Integer> journalPublications = new HashMap<>();

        @Override
        protected void map(Object key, Group value, Context context) throws IOException, InterruptedException {
            String journal = value.getString(COLUMN_JOURNAL, 0);
            if(journal!=null){
                journalWritable.set(journal);
                context.write(journalWritable, one);
                // UNCOMMENT FOR IN-MAPPER COMBINER
                /*if(journalPublications.containsKey(journal)){
                    int sum = journalPublications.get(journal) + 1;
                    journalPublications.put(journal, sum);
                }else {
                    journalPublications.put(journal, 1);
                }*/
            }
        }

        //UNCOMMENT FOR IN-MAPPER COMBINER
        /*
        public void cleanup(Context context) throws IOException, InterruptedException {
            Iterator<Map.Entry<String, Integer>> temp = journalPublications.entrySet().iterator();

            while(temp.hasNext()) {
                Map.Entry<String, Integer> entry = temp.next();
                String keyVal = entry.getKey();
                Integer countVal = entry.getValue();
                context.write(new Text(keyVal), new IntWritable(countVal));
            }
        }*/
    }

    static class OccurrencesSumReducer extends Reducer<Text, IntWritable, Text,IntWritable> {

        private IntWritable sumWritable = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            sumWritable.set(sum);
            context.write(key, sumWritable);
        }
    }

}
