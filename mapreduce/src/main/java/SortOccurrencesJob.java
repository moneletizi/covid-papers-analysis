import com.google.common.base.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.lib.IdentityReducer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.parquet.hadoop.example.ExampleInputFormat;

import java.io.IOException;

public class SortOccurrencesJob {

    private static final Log LOGGER = LogFactory.getLog(SortOccurrencesJob.class);
    private static final String JOB_NAME = "SortJob";

    public static Job getJob(Configuration conf, Path inputPath, Path outputPath, boolean descending, Optional<Integer> limit) throws IOException {
        LOGGER.info("GetJob" + JOB_NAME);
        Job job = Job.getInstance(conf, JOB_NAME);

        // Clear output folder
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputPath)) {
            LOGGER.info(JOB_NAME+" - Deleting already existing output file");
            hdfs.delete(outputPath, true);
        }

        job.setInputFormatClass(KeyValueTextInputFormat.class);
        job.setJarByClass(SortOccurrencesJob.class);

        // set mapper, reducer and combiner class
        job.setMapperClass(SwapMapper.class);
        // set limit parameter
        if(limit.isPresent()){
            job.getConfiguration().setInt("limit", limit.get());
            job.setReducerClass(LimitReducer.class);
            job.setNumReduceTasks(1);
        }else{
            //set the identity reducer
            job.setReducerClass(IdentityReducer.class);
            job.setNumReduceTasks(1);
        }

        if(descending){
            job.setSortComparatorClass(DescendingIntComparator.class);
        }

        // set output (k,v) type
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);

        // set input and output path
        FileInputFormat.addInputPath(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);

        return job;
    }

    static class SwapMapper extends Mapper<Text, Text, IntWritable, Text> {
        private IntWritable occurrencesWritable = new IntWritable();
        @Override
        public void map(Text journal, Text occurrences, Context context) throws IOException, InterruptedException {
            occurrencesWritable.set(Integer.parseInt(occurrences.toString()));
            context.write(occurrencesWritable, journal);
        }
    }

    static class LimitReducer extends Reducer<IntWritable,Text,Text,IntWritable> {

        private int keyProcessed = 0;

        public void reduce(IntWritable key,Iterable<Text> values, Context context) throws IOException, InterruptedException {
            for(Text value : values){
                LOGGER.info("processed key " + keyProcessed + "in reduce phase");
                int limit = context.getConfiguration().getInt("limit", -1);
                if (limit==-1) LOGGER.error("WRONG LIMIT SET IN CONFIGURATION PARAMETERS");
                if(keyProcessed < limit){
                    context.write(value, key);
                    keyProcessed++;
                }
            }
        }
    }

    static class IdentityReducer extends Reducer<IntWritable,Text,Text,IntWritable> {
        public void reduce(IntWritable key,Iterable<Text> values, Context context) throws IOException, InterruptedException {
            for(Text value : values) {
                context.write(value, key);
            }
        }
    }

    //custom comparator for descending order
    static class DescendingIntComparator extends WritableComparator {

        public DescendingIntComparator() {
            super(IntWritable.class, true);
        }

        @Override
        public int compare(WritableComparable w1, WritableComparable w2) {
            IntWritable key1 = (IntWritable) w1;
            IntWritable key2 = (IntWritable) w2;
            return -1 * key1.compareTo(key2);
        }
    }

}
