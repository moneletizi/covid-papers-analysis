import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.example.ExampleInputFormat;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;

import static utils.DatasetFieldsConstants.COLUMN_AUTHOR;
import static utils.DatasetFieldsConstants.COLUMN_JOURNAL;
import static utils.InputOutputPaths.*;

public class CountPublicationsPerAuthorReplicatedJoinJob {

    private static final Log LOGGER = LogFactory.getLog(CountPublicationsPerAuthorReplicatedJoinJob.class);
    private static final String JOB_NAME = "CountPublicationsPerAuthorReplicatedJoinJob";

    public static Job getJob(Configuration conf, int numberReduceTasks,Path inputPath, Path outputPath, boolean useCombiner) throws IOException, URISyntaxException {
        LOGGER.info("GetJob" + JOB_NAME);
        Job job = Job.getInstance(conf, JOB_NAME);

        // Clear output folder
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputPath)) {
            LOGGER.info(JOB_NAME + " - Deleting already existing output file");
            hdfs.delete(outputPath, true);
        }

        // Read parquet format
        job.setInputFormatClass(ExampleInputFormat.class);
        job.setJarByClass(CountPublicationsPerAuthorReplicatedJoinJob.class);

        // set number of reducer tasks
        job.setNumReduceTasks(numberReduceTasks);

        try{
            DistributedCache.addCacheFile(new Path(CL_OUTPUT_TXT_COUNTING_SORTED+"/part-r-00000").toUri(), job.getConfiguration());
            LOGGER.info("topJournalFile added in cache");
        }
        catch (Exception e) {
            LOGGER.error("Error while writing cache file");
        }

        job.setMapperClass(MapperMapSideJoin.class);
        if(useCombiner){
            job.setCombinerClass(OccurrencesSumReducer.class);
        }
        job.setReducerClass(OccurrencesSumReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        // set output (k,v) type
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);

        return job;
    }

    static class MapperMapSideJoin extends Mapper<Object, Group, Text, IntWritable> {

        //UNCOMMENT FOR IN-MAPPER COMBINER
        //private Map<String, Integer> authorPublications = new HashMap<>();

        public static final String CL_IMPORTANT_JOURNALS_RESULT_FILE = "/user/sletizi/examProject/mapreduce/output/CountingSortedOutput.txt/part-r-00000";
        private static Set<String> journalSet = new HashSet<>();
        private BufferedReader brReader;
        private final static IntWritable one = new IntWritable(1);
        private Text authorWritable = new Text();

        //UNCOMMENT FOR IN-MAPPER COMBINER
        /*public void cleanup(Context context) throws IOException, InterruptedException {
            Iterator<Map.Entry<String, Integer>> temp = authorPublications.entrySet().iterator();
            while(temp.hasNext()) {
                Map.Entry<String, Integer> entry = temp.next();
                String keyVal = entry.getKey();
                Integer countVal = entry.getValue();
                context.write(new Text(keyVal), new IntWritable(countVal));
            }
        }*/

        //the mapper loads cached file in lookup table
        @Override
        protected void setup(Context context) throws IOException {
            String strLineRead = "";
            Path path = new Path(CL_IMPORTANT_JOURNALS_RESULT_FILE);
            try{
                String stopwordCacheName = path.getName();
                Path[] cacheFiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());
                if (cacheFiles.length > 0) {
                    for (Path cachePath : cacheFiles) {
                        if (cachePath.getName().equals(stopwordCacheName)) {
                            LOGGER.info(JOB_NAME + " - File "+ path.getName());
                            loadJournals(cachePath);
                            break;
                        }
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("File not found");
            } catch (IOException e) {
                System.out.println("Some other error");
                e.printStackTrace();
            }finally {
                if (brReader != null) {
                    brReader.close();
                }

            }
        }

        private void loadJournals(Path cachePath) throws IOException {
            // note use of regular java.io methods here - this is a local file now
            BufferedReader wordReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(cachePath.toString()))));
            try {
                String line;
                while ((line = wordReader.readLine()) != null) {
                    String[] rowValue = line.split("\\t");
                    journalSet.add(rowValue[0]);
                }
            } finally {
                wordReader.close();
            }
        }

        @Override
        protected void map(Object key, Group value, Context context) throws IOException, InterruptedException {
            if(journalSet.contains(value.getString(COLUMN_JOURNAL, 0))){
                String authorsString = value.getString(COLUMN_AUTHOR,0);
                for(String author : authorsString.split(",")){
                    if(author!=null){
                        //UNCOMMENT FOR IN-MAPPER COMBINER
                        /*if(authorPublications.containsKey(author)){
                            int sum = authorPublications.get(author) + 1;
                            authorPublications.put(author, sum);
                        }else {
                            authorPublications.put(author, 1);
                        }*/
                        authorWritable.set(author);
                        context.write(authorWritable,one);
                    }
                }
            }
        }

    }

    static class OccurrencesSumReducer extends Reducer<Text, IntWritable, Text,IntWritable> {

        private IntWritable sumWritable = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            sumWritable.set(sum);
            context.write(key, sumWritable);
        }
    }
}
