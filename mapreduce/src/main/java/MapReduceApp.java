import com.google.common.base.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import static utils.InputOutputPaths.*;
import java.util.ArrayList;

// launch mode without specifying # of reducer : hadoop jar BDE-mr-letizi-all.jar MapReduceApp
// launch mode specifying reducer : hadoop jar BDE-mr-letizi-all.jar MapReduceApp <numReducerFirstJob> <numReducerTaskThirdJob> <useCombiner>

public class MapReduceApp {
    private static final Log LOGGER = LogFactory.getLog("Application");

    public static void main(String[] args) throws Exception{

        Path completeDatasetInputParquet = new Path(CL_DATASET_PARQUET_CLEANED);
        Path outputCountingJobPath = new Path(CL_OUTPUT_TXT_COUNTING);
        Path outputSortJobPath = new Path(CL_OUTPUT_TXT_COUNTING_SORTED);
        Path disorderedAuthorsPubOutputPath = new Path(CL_OUTPUT_TXT_AUTHORS_PUBS);
        Path orderedAuthorsPubOutputPath = new Path(CL_OUTPUT_TXT_AUTHORS_PUBS_SORTED);

        int numReducerTaskFirstJob = args.length > 0  && Integer.parseInt(args[0])> 0 ? Integer.parseInt(args[0]) : 1;
        int numReducerTaskThirdJob = args.length > 1 && Integer.parseInt(args[1])> 0 ? Integer.parseInt(args[1]) : 1;
        boolean useCombiner = args.length > 2 && Boolean.parseBoolean(args[2]);

        LOGGER.info("num reducer for CountPublicationsPerJournalJob job : " + numReducerTaskFirstJob);
        LOGGER.info("num reducer for CountPublicationsPerAuthorReplicatedJoinJob job : " + numReducerTaskThirdJob);
        LOGGER.info("combiner : " + useCombiner);

        ArrayList<Job> jobs = new ArrayList<>();
        jobs.add(CountPublicationsPerJournalJob.getJob(new Configuration(), numReducerTaskFirstJob, completeDatasetInputParquet, outputCountingJobPath, useCombiner));
        jobs.add(SortOccurrencesJob.getJob(new Configuration(), outputCountingJobPath, outputSortJobPath, true, Optional.of(100)));
        jobs.add(CountPublicationsPerAuthorReplicatedJoinJob.getJob(new Configuration(), numReducerTaskThirdJob, completeDatasetInputParquet, disorderedAuthorsPubOutputPath, useCombiner));
        jobs.add(SortOccurrencesJob.getJob(new Configuration(), disorderedAuthorsPubOutputPath, orderedAuthorsPubOutputPath, true, Optional.<Integer>absent()));

        for (Job job: jobs) {
            if (!job.waitForCompletion(true)) {
                System.exit(1);
            }
        }
    }
}
