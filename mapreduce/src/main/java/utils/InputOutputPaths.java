package utils;

public final class InputOutputPaths {
    // Virtual Machine Paths
    //public static final String DATASET_PARQUET = "hdfs:/bigdata/datasets/proj/COVID-Literature-Analysis.parquet";
    //public static final String DATASET_SUBSET_PARQUET = "hdfs:/bigdata/datasets/proj/subset-covid-from-JSON.parquet";
    //public static final String OUTPUT_TXT_COUNTING = "hdfs:/project/mapreduce/output/CountingOutput.txt";
    //public static final String OUTPUT_TXT_COUNTING_SORTED = "hdfs:/project/mapreduce/output/CountingSortedOutput.txt";
    //public static final String OUTPUT_TXT_AUTHORS_PUBS = "hdfs:/project/mapreduce/output/AuthorsPubsOutput.txt";
    //public static final String OUTPUT_TXT_AUTHORS_PUBS_SORTED = "hdfs:/project/mapreduce/output/AuthorsPubsSortedOutput.txt";
    //public static final String OUTPUT_TXT_ORDERING_JOB = "hdfs://bigdata/datasets/proj/OrderingJobOutput.txt";

    //Cluster paths
    public static final String CL_DATASET_PARQUET_CLEANED = "hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet";
    public static final String CL_OUTPUT_TXT_COUNTING = "hdfs:/user/sletizi/examProject/mapreduce/output/CountingOutput.txt";
    public static final String CL_OUTPUT_TXT_COUNTING_SORTED = "hdfs:/user/sletizi/examProject/mapreduce/output/CountingSortedOutput.txt";
    public static final String CL_OUTPUT_TXT_AUTHORS_PUBS = "hdfs:/user/sletizi/examProject/mapreduce/output/AuthorsPubsOutput.txt";
    public static final String CL_OUTPUT_TXT_AUTHORS_PUBS_SORTED = "hdfs:/user/sletizi/examProject/mapreduce/output/AuthorsPubsSortedOutput.txt";
    
    //AWS Paths

}
