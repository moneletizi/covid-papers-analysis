package utils;

public final class DatasetFieldsConstants {

    private DatasetFieldsConstants() {}

    //dataset fields
    public static final String COLUMN_PAPER_ID = "paper_id";
    public static final String COLUMN_ABSTRACT = "abstract";
    public static final String COLUMN_BODY_TEXT = "body_text";
    public static final String COLUMN_AUTHOR = "authors";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_JOURNAL = "journal";
    public static final String COLUMN_ABSTRACT_SUMMARY = "abstract_summary";
    public static final String COLUMN_LABELS = "Labels";

}
