package converter

import org.apache.spark.sql.SparkSession

object ConvertFromJSONToPartitionedParquet {

  def main (args : Array[String]) {

    val spark = SparkSession.builder().appName("ConvertFromJSONToPartitionedParquet").getOrCreate()

    import org.apache.spark.sql.SaveMode
    import org.apache.spark.sql.types.{StringType, StructType}
    val schema = new StructType()
      .add("paper_id", StringType, nullable = true)
      .add("abstract", StringType, nullable = true)
      .add("body_text", StringType, nullable = true)
      .add("authors", StringType, nullable = true)
      .add("title", StringType, nullable = true)
      .add("journal", StringType, nullable = true)
      .add("abstract_summary", StringType, nullable = true)
      .add("Labels", StringType, nullable = true)

    val dfcovid = spark.read.format("json").schema(schema).json("hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned.json").cache() //WORKING PATH ON CLUSTER -> hdfs:///user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned.json
    dfcovid.count()

    dfcovid.coalesce(args(2).toInt).write.mode(SaveMode.Overwrite).parquet(args(1))

  }
}
