package converter

import org.apache.spark.sql.SparkSession

//CLUSTER: spark2-submit --class ConvertFromCSVToParquet BDE-spark-letizi-all.jar hdfs:///user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned.csv hdfs:/user/sletizi/examProject/dataset/<FileName>.parquet
/*
    Spark2 implementation
 */
object ConvertFromCSVToParquet {

    def main (args : Array[String]) {

        val appName = "ConvertFromCSVToParquet"

        /* Spark1 implementation
        val rdd = spark.sparkContext.textFile("hdfs:/bigdata/datasets/proj/COVID-Literature-Analysis_cleaned.csv").map(line=>line.split(",")).map(e ⇒ Row(e(0), e(1), e(2), e(3), e(4), e(5), e(6), e(7)))
        val filtered = rdd.filter(row => row.getAs[String](3)!=null && row.getAs[String](5)!=null && row.getAs[String](6)!=null && row.getAs[String](7)!=null)*/

        //val df = sqlContext.createDataFrame(filtered, schema)

        val spark = SparkSession.builder().appName(appName).getOrCreate()

        import org.apache.spark.sql.SaveMode
        import org.apache.spark.sql.types.{StringType, StructType}
        val schema = new StructType().add("paper_id", StringType, true)
                                      .add("abstract", StringType, true)
                                      .add("body_text", StringType, true)
                                      .add("authors", StringType, true)
                                      .add("title", StringType, true)
                                      .add("journal", StringType, true)
                                      .add("abstract_summary", StringType, true)
                                      .add("Labels", StringType, true)

        val df = spark.read.option("header","true").option("escape", "\r").schema(schema).csv(args(0))

        df.write.mode(SaveMode.Overwrite).parquet(args(1))

    }
}
