package converter

import org.apache.spark.sql.SparkSession

object ConvertFromJSONToParquet {

  def main (args : Array[String]) {

    val spark = SparkSession.builder().appName("ConvertFromJSONToParquet").getOrCreate()

    import org.apache.spark.sql.SaveMode
    import org.apache.spark.sql.types.{StringType, StructType}
    val schema = new StructType()
      .add("paper_id", StringType, true)
      .add("abstract", StringType, true)
      .add("body_text", StringType, true)
      .add("authors", StringType, true)
      .add("title", StringType, true)
      .add("journal", StringType, true)
      .add("abstract_summary", StringType, true)
      .add("Labels", StringType, true)

    val dfcovid = spark.read.format("json").schema(schema).json(args(0)).cache()

    dfcovid.first()

    dfcovid.write.mode(SaveMode.Overwrite).parquet(args(1))

  }
}
