package utils

import org.apache.spark.sql.Row

object PublicationData {
  def extract(row:Row) = {
    PublicationData(row.getAs[String]("paper_id"),row.getAs[String]("authors"),row.getAs[String]("journal"),row.getAs[String]("abstract_summary"),row.getAs[String]("Labels"))
  }
}

case class PublicationData(paper_id:String,
                           authors:String,
                           journal:String,
                           abstract_summary:String,
                           Labels:String)