package query2

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{HashPartitioner, SparkConf}
import utils.PublicationData

// CLUSTER: spark2-submit --class query2.MeanFreqWordPerCategory BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/dataset/stopwords.txt
object MeanFreqWordPerCategory {

  def main (args : Array[String]) {
      val inputPath = args(0)
      val stopWordsFilePath = args(1)

      val spark = SparkSession.builder().appName("Mean Word Frequency per Category").getOrCreate()

      //send in broadcast stop words file
      val stopWordsSet = spark.sparkContext.textFile(stopWordsFilePath).collect().toSet //load stop word file
      val broadcastStopWords = spark.sparkContext.broadcast(stopWordsSet) //exploit broadcast variables and send stop word file to executors

      val rdd = spark.read.format("parquet").load(inputPath).drop("abstract","body_text","title","authors","journal") //drop unused columns
                                                    //.repartition(16)
                                                    .filter(row => row.getAs[String]("Labels")!=null && row.getAs[String]("abstract_summary")!=null && row.getAs[String]("abstract_summary")!="Not provided.")
                                                    .rdd
                                                    .map(PublicationData.extract)
                                                    //.cache()
      //rdd.count()

      // UNITO
      val paperPerCat = rdd.map(paperData => (paperData.Labels,1)).reduceByKey(_+_).collectAsMap() // create pairs (Label, 1) and reduceByKey : obtain #papers per Label
      val broadcastPaperPerCat = spark.sparkContext.broadcast(paperPerCat) //send in broadcast to executors

      val wordsPerPublication = rdd.map(pub => (pub, pub.abstract_summary))
                        .flatMapValues(abstract_summary => abstract_summary.split(" "))
                        .map({case (publicationData,word) => ((publicationData.Labels, publicationData.paper_id),word.toLowerCase)})
                        .filter({case (_,word) => !broadcastStopWords.value.contains(word)})
                        //.keyBy(_._1.Labels)
                        .partitionBy(new HashPartitioner(12))
                        .persist(StorageLevel.MEMORY_ONLY_SER)

      //count to enforce caching
      wordsPerPublication.count()

      val paperLengthRDD = wordsPerPublication.aggregateByKey(0)((acc,v)=>acc+1,_+_)
      val broadcastPaperLength = spark.sparkContext.broadcast(paperLengthRDD.collectAsMap())

      val wordOccPerPaperRDD = wordsPerPublication.map({case ((label, paper_id), word) => ((label, paper_id, word) ,1)})
                            .reduceByKey(_+_) //total word occurrences per paper
                            .cache() //cached reused RDD
      wordOccPerPaperRDD.count() //count to enforce caching

      val weightedMeanFreqRDD = wordOccPerPaperRDD.map({case ((label,paper,word),occurrences) => ((label, word),occurrences)})
                                                    .reduceByKey(_+_)
                                                    .map({case (labelAndWord, totalOccurrences) => (labelAndWord._1,(labelAndWord._2, totalOccurrences.toDouble / broadcastPaperPerCat.value.getOrElse(labelAndWord._1, 1)))})

      val meanWordTFPerLabel = wordOccPerPaperRDD.map({case ((label, paper,word),occurrences) => ((label,word),occurrences.toDouble / broadcastPaperLength.value.getOrElse((label,paper),1))}) // TF
                                                .reduceByKey(_+_)
                                                .map({case (labelAndWord, totalOccurrences) => (labelAndWord._1,(labelAndWord._2, totalOccurrences.toDouble / broadcastPaperPerCat.value.getOrElse(labelAndWord._1, 1)))})

      println(weightedMeanFreqRDD.groupByKey().mapValues(iterable => iterable.toList.sortBy(_._2)(Ordering[Double].reverse).take(10)))


      println(meanWordTFPerLabel.groupByKey().mapValues(iterable => iterable.toList.sortBy(_._2)(Ordering[Double].reverse).take(10)))

  }
}
