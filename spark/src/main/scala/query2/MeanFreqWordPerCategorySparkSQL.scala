package query2

import org.apache.spark.SparkConf
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructType}
import utils.PublicationData

// spark2-submit --class query2.MeanFreqWordPerCategorySparkSQL BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/sparkSQL/output/WordMeanTFPerCategory.csv hdfs:/user/sletizi/examProject/sparkSQL/output/WordMeanOccurrencesPerCategory.csv hdfs:/user/sletizi/examProject/dataset/stopwords.txt
object MeanFreqWordPerCategorySparkSQL {

  def main (args : Array[String]) {
      val inputPath = args(0)
      val outputMeanTFPath = args(1)
      val outputMeanOccPath = args(2)
      val stopWordsFilePath = args(3)

      val config =  new SparkConf().setAppName("Mean Word Frequency per Category - SparkSQL")
                                    .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                                    .set("spark.sql.shuffle.partitions","12")
                                    .registerKryoClasses(Array(classOf[PublicationData]))

      val spark = SparkSession.builder().config(config).getOrCreate()

      //load stop words file in DF
      val stopWordsDF = spark.read.option("header","false").schema(new StructType().add("word", StringType, false)).csv(stopWordsFilePath)

      //load complete dataset and clean it removing unused columns and remove records with null values
      val cleanDataset = spark.read.format("parquet").load(inputPath)
                                                            .drop("abstract","body_text","title","authors","journal")
                                                            .filter(row => row.getAs[String]("Labels")!=null && row.getAs[String]("abstract_summary")!=null && row.getAs[String]("abstract_summary")!="Not provided.")

      //count number of papers per category
      val paperPerLabel = cleanDataset.groupBy("Labels").agg(count("*").as("paperPerLabel"))

      // create one row for each word in a paper and map in lowerCase
      val extendedDf = cleanDataset.withColumn("wordCaseSens", explode(split(col("abstract_summary"), " "))).drop("abstract_summary")
                                      .withColumn("word", lower(col("wordCaseSens"))).drop("wordCaseSens").filter(row => !row.getAs[String]("word").isEmpty)

      // filter stop words (obtain a DF with one row per word in the paper)
      val filteredWord = extendedDf.join(broadcast(stopWordsDF),extendedDf("word")===stopWordsDF("word"),"leftanti")
                                    .repartition(12,col("paper_id")).cache()
      filteredWord.count()

      //count the paper length
      val paperLength =  filteredWord.groupBy("paper_id").agg(count("*").as("paperLength"))

      //count the word occurrences in a paper
      val wordOccInPaper = filteredWord.groupBy("paper_id","word", "Labels").agg(count("word").as("Occurrences")).cache()
      wordOccInPaper.count()

      // join the paper length column with the word occurrences and calculate the Term Frequency (TF) for each word in a paper
      val joinedWithPaperLength = wordOccInPaper.join(broadcast(paperLength), "paper_id")
      val wordsTF = joinedWithPaperLength.withColumn("T-F",expr("Occurrences/paperLength")).select("word","T-F","Labels", "paper_id")

      /*
          join the words TF table with the # of papers per category (in order to calculate the average correctly)
          there may be papers in a the category that do not contain a word so there will be no line of that paper; in this way the average will still be calculated correctly
       */
      val joined2 = wordsTF.join(broadcast(paperPerLabel), "Labels")
      val finalTable = joined2.repartition(col("Labels")).groupBy("Labels","word","paperPerLabel").agg(sum("T-F").as("totalTF")).select(col("Labels"),col("word"),expr("totalTF/paperPerLabel").as("mean-TF"))

      // the mean-TF is sorted exploiting Window functions
      val window = Window.partitionBy(col("Labels"))
      val rankTable = finalTable.select(col("*"), rank().over(window.orderBy(col("mean-TF").desc)).alias("rank")).filter(col("rank") <= 10)

      rankTable.coalesce(1).write.mode(SaveMode.Overwrite).csv(outputMeanTFPath)

      //if we want query also the average occurrences of word per label
      val totalOccurrencesInLabel = wordOccInPaper.groupBy("Labels","word").agg(count("word").as("totalWordOccurrencesInLabel"))
      val totalOccurrencesInLabelAndPapersNum =  totalOccurrencesInLabel.join(broadcast(paperPerLabel), "Labels")
      val rankOccurrencesTable = totalOccurrencesInLabelAndPapersNum.select(col("Labels"),col("word"), expr("totalWordOccurrencesInLabel/paperPerLabel").as("meanOccurrencesPerLabel"))
      val orderedRankMeanOccurrences = rankOccurrencesTable.select(col("*"), rank().over(window.orderBy(col("meanOccurrencesPerLabel").desc)).alias("rank")).filter(col("rank") <= 10)

      orderedRankMeanOccurrences.coalesce(1).write.mode(SaveMode.Overwrite).csv(outputMeanOccPath)


  }

}
