package query1

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.rdd.{PairRDDFunctions, RDD}
import org.apache.spark.sql.SparkSession
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}
import utils.PublicationData
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.SparkContext._
//run command
// CLUSTER: spark2-submit --class query1.AuthorsRankJobSpark BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/spark/output/authors_rank.txt
object AuthorsRankJobSpark {

    def main(args : Array[String]) {
        val inputPath = args(0)
        val outputPath = args(1)
        val solution = if(args.length > 2) args(2) else "2"
        val numPartitions = if(args.length > 3) args(3).toInt else 12
        val cache =  if(args.length > 4) args(4).toBoolean else false
        // if no parameters are specified, are used by default: partitioning, no caching and broadcast variables

        /* config = new SparkConf().setAppName("Authors Rank Spark")
              .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
              .registerKryoClasses(Array(classOf[PublicationData]))*/

        //val logger: Logger = Logger.getLogger("BDE Spark Letizi Logger")

        val spark = SparkSession.builder().appName("Authors Rank Spark").getOrCreate()

        //clean output folder
        val hdfs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
        val outputCountingJobPath = new Path(outputPath)
        if (hdfs.exists(outputCountingJobPath)) hdfs.delete(outputCountingJobPath, true)

        val usePartitioner : Option[Int] = numPartitions match {
            case 0 => None
            case n => Some(n)
        }

        solution match {
            case "1" => JoinSolution(spark, inputPath, outputPath, usePartitioner, cache)
            case "2" => broadcastSolution(spark, inputPath, outputPath, usePartitioner, cache)
            case "3" => broadcastWithMapInsteadOfKeyBy(spark, inputPath, outputPath)
        }
    }

    def getPartitionedOrCachedRDD(RDDJournalKey: RDD[(String, PublicationData)], usePartitioner: Option[Int], cache: Boolean) : RDD[(String, PublicationData)] = (usePartitioner, cache) match {
        case (Some(numPartition), true) => {
            val partitionedCached = RDDJournalKey.partitionBy(new HashPartitioner(numPartition)) //put records with the same journal value in the same partition
                                    .cache() // cache RDD reused more times
            partitionedCached.count() //count to enforce partitioning
            partitionedCached

        }
        case (Some(numPartition), false) => RDDJournalKey.partitionBy(new HashPartitioner(numPartition))
        case (None, true) => {
            val cached = RDDJournalKey.cache() // cache RDD reused more times
            cached.count() //count to enforce partitioning
            cached
        }
        case (None, false) =>  RDDJournalKey
    }

    def JoinSolution(spark: SparkSession, inputPath: String, outputPath: String, usePartitioner: Option[Int], cache: Boolean) {

        val cleanedRDD = loadCleanAndWrapRDD(spark, inputPath)

        val RDDJournalKey = cleanedRDD.keyBy(_.journal)

        val RDDJournalKeyCachedOrPartitioned = getPartitionedOrCachedRDD(RDDJournalKey, usePartitioner, cache)

        val topJournalArray = countByKeyAndTakeFirstN(RDDJournalKeyCachedOrPartitioned, 100)

        val topJournalsRDD = spark.sparkContext.parallelize(topJournalArray) // transform the array in RDD

        //topJournalsRDD.coalesce(1).saveAsTextFile("hdfs:/user/sletizi/examProject/spark/output/journals_rank.txt")

        val rddJournalAuthorRDD = RDDJournalKeyCachedOrPartitioned.join(topJournalsRDD)
                                        .flatMapValues({case (publicationData, journalOccurrences)  => publicationData.authors.split(",")}) //split authors field

        val authorsRank = countAndSortAuthorPublications(rddJournalAuthorRDD)

        saveRDD(authorsRank, outputPath)

    }


    def broadcastSolution(spark: SparkSession, inputPath: String, outputPath: String, usePartitioner: Option[Int], cache: Boolean): Unit ={
        val cleanedRDD = loadCleanAndWrapRDD(spark, inputPath)
        val RDDJournalKey = cleanedRDD.keyBy(_.journal)

        val RDDJournalKeyCachedOrPartitioned = getPartitionedOrCachedRDD(RDDJournalKey, usePartitioner, cache)

        val topJournalsArray = countByKeyAndTakeFirstN(RDDJournalKeyCachedOrPartitioned, 100).toMap

        val broadcastTopJournal = spark.sparkContext.broadcast(topJournalsArray)

        val rddJournalAuthorRDD = RDDJournalKeyCachedOrPartitioned.filter({case (journal, pub) => broadcastTopJournal.value.contains(journal)})
                                                        .flatMapValues(pub => pub.authors.split(","))

        val authorsRank = countAndSortAuthorPublications(rddJournalAuthorRDD)

        saveRDD(authorsRank, outputPath)
    }

    def broadcastWithMapInsteadOfKeyBy(spark: SparkSession, inputPath: String, outputPath: String): Unit = {
        val rdd = loadCleanAndWrapRDD(spark, inputPath)

        val topJournalsArray = rdd.map(pub => (pub.journal,1))
          .reduceByKey(_+_)
          .takeOrdered(100)(Ordering[Int].reverse.on(x=>x._2)).toMap

        val broadcastTopJournal = spark.sparkContext.broadcast(topJournalsArray)

        val rddAuthorsRank = rdd.filter(pub => broadcastTopJournal.value.contains(pub.journal))
          .flatMap(pub => pub.authors.split(","))
          .map(author => (author, 1))
          .reduceByKey(_+_)
          .sortBy(_._2, false)
          .coalesce(1)
          .saveAsTextFile(outputPath)

    }

    def loadCleanAndWrapRDD(spark: SparkSession, inputPath : String): RDD[PublicationData] ={
        spark.read.format("parquet").load(inputPath).drop("abstract_full","body_text","title") // loead file and removed unused columns
                                          .rdd // convert DF to RDD
                                          .filter(row=>row.getAs("authors")!=null && row.getAs("journal")!=null) // remove row with null value for the fields
                                          .map(PublicationData.extract)
    }

    def  countByKeyAndTakeFirstN (RDD: RDD[(String,PublicationData)], n:Int) = {
        RDD.mapValues(publicationData=> 1) //mapValues maintain partitioning criteria, if it was set
          .reduceByKey(_+_) // reduce compute locally if partitioning criteria was set
          .takeOrdered(n)(Ordering[Int].reverse.on(x=>x._2)) //take first 100 elements and sort descending by value
    }

    def countAndSortAuthorPublications(RDDJournalAuthor : RDD[(String, String)]) = {
        RDDJournalAuthor.map({ case (journal,author) => (author, 1)}) //change key
                      .reduceByKey(_+_) //reduce by author key
                      .sortBy(_._2, false) //sort descending by value
    }

    def saveRDD(RDD: RDD[(String, Int)], outputPath:String) = {
        RDD.coalesce(1) //reduce the number of partitions to get a single output file
            .saveAsTextFile(outputPath)
    }

}
