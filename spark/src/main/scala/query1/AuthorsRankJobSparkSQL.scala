package query1

import org.apache.spark.SparkConf
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.storage.StorageLevel
import utils.PublicationData

//spark2-submit --class query1.AuthorsRankJobSparkSQL --num-executors 2 --executor-cores 3 BDE-spark-letizi-all.jar hdfs:/user/sletizi/examProject/dataset/COVID-Literature-Analysis_cleaned_partitioned.parquet hdfs:/user/sletizi/examProject/sparkSQL/output/authors_rank_spark.txt
object AuthorsRankJobSparkSQL {
  def main (args : Array[String]) {
    val inputPath = args(0)
    val outputPath = args(1)

    //val logger: Logger = Logger.getLogger("BDE Spark Letizi Logger")

    val config =  new SparkConf().setAppName("Authors Rank SparkSQL")
                                  .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                                  .set("spark.sql.shuffle.partitions","12") //Spark set 200 partitions by default
                                  .registerKryoClasses(Array(classOf[PublicationData]))

    val spark = SparkSession.builder().config(config).getOrCreate()

    val partitionedDF = spark.read.format("parquet")
                                  .load(inputPath)
                                  .drop("abstract_full","body_text","title")
                                  .filter(row=>row.getAs("authors")!=null && row.getAs("journal")!=null)
                                  .repartition(12, col("journal"))
                                  .persist(StorageLevel.MEMORY_ONLY_SER)
    partitionedDF.count()

      val topJournal = partitionedDF.groupBy("journal")
                                    .agg(count("journal").as("journalPubs"))
                                    .sort(col("journalPubs").desc)
                                    .limit(100)

      //topJournal.coalesce(1).write.mode(SaveMode.Overwrite).csv("hdfs:/user/sletizi/examProject/sparkSQL/output/topJournals.csv")

      val authorsRank = partitionedDF.join(broadcast(topJournal),topJournal("journal")===partitionedDF("journal"))
                                    .withColumn("author", explode(split(col("authors"), ","))).drop("authors")
                                    .groupBy("author")
                                    .agg(count("author").as("#publications"))
                                    .sort(desc("#publications"))

      authorsRank.coalesce(1).write.mode(SaveMode.Overwrite).csv(outputPath)

    //You can use hints (Spark >= 2.2):
    //spark.sql("/*+ broadcast(topJournal) */ select p.authors,count(*) as authorPubs from publications_author_split p, topJournal tj where p.journal IN tj.journal group by p.author order by desc p.author")

  }
}
